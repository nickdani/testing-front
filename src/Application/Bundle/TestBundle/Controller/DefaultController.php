<?php

namespace Application\Bundle\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $token = $this->container->getParameter('api_token');

        function token($token)
        {
            $url = "http://testing-api.dev/access/token/" . $token;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            if (curl_errno($ch)) {
                return curl_error($ch);
            }
            curl_close($ch);

            return json_decode($response)->token;
        }

        $token = token($token);


        function secret($token)
        {
            $url = "http://testing-api.dev/secret/" . $token;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            if (curl_errno($ch)) {
                return curl_error($ch);
            }
            curl_close($ch);

            return json_decode($response, true);
        }

        $secret = secret($token);

        return $this->render('ApplicationTestBundle:Default:index.html.twig', ['secret' => $secret['secret']]);
    }
}
