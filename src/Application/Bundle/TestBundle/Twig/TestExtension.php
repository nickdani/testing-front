<?php

namespace Application\Bundle\TestBundle\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class PaymentsExtension
 */
class TestExtension extends \Twig_Extension
{
    public $code = 'ILOVESYMFONY';
    /**
     * Configs for payment methods
     *
     * @var array
     */
    protected $methodConfig;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var Session
     */
    protected $session;

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('show_something_interesting', [$this, 'getInteresting']),
        );
    }

    public function getInteresting($secret)
    {
        function decrypt($decrypt, $key){
            $decrypt = explode('|', $decrypt.'|');
            $decoded = base64_decode($decrypt[0]);
            $iv = base64_decode($decrypt[1]);
            if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
            $key = hex2bin($key);
            $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
            $mac = substr($decrypted, -64);
            $decrypted = substr($decrypted, 0, -64);
            $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
            if($calcmac!==$mac){ return false; }
            $decrypted = unserialize($decrypted);
            return $decrypted;
        }

        function hex2bin($hexdata) {
            $bindata="";
            for ($i=0;$i<strlen($hexdata);$i+=2) {
                $bindata.=chr(hexdec(substr($hexdata,$i,2)));
            }

            return $bindata;
        }

        return decrypt($secret, $this->code);

    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_test';
    }
}
